import getGiphy from '../external-api/giphy';
import getRecipesPuppy from '../external-api/recipePuppy';
import splitAndOrder from '../utils/string';

const getRecipesResult = async (params) => {
  const recipes = await getRecipesPuppy(params);

  const recipesResult = await Promise.all(
    recipes.map(async (recipe) => {
      const gif = await getGiphy(recipe.title);

      return {
        title: recipe.title,
        ingredients: splitAndOrder(recipe.ingredients, ','),
        link: recipe.href,
        gif,
      };
    }),
  );

  return recipesResult;
};

export default getRecipesResult;
