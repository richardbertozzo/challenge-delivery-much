import { get } from 'axios';

const getRecipesPuppy = async (params) => {
  try {
    const response = await get(`http://www.recipepuppy.com/api/?i=${params.i}`);
    return response.data.results;
  } catch (error) {
    throw new Error('External API unavailable');
  }
};

export default getRecipesPuppy;
