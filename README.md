## Challenge for Delivery Much

Service to return a list of recipes from up to 3 ingredients. Using two others APIs [Recipe Puppy](http://www.recipepuppy.com/about/api/) and [Giphy](https://developers.giphy.com/docs/).

## Requirements

- Docker

## Running the API within Docker
- Clone this repo

- Generate a API key of Giphy - [Generate a key for Giphy API](https://developers.giphy.com/dashboard/?create=true)

* Create a `.env` file using as `.env.example` as an example. So, put the key from the Giphy API in the variable env file.

* `make build` to build the container

* `make run` to run the container, is expose on port 3000

Now you have a API running on port 3000

#### Example of GET in API

http://localhost:3000/recipes/?i=bread,bacon,cheese - Get recipes with 3 ingredients(bread, bacon, cheese).

## Running the API with NPM
- Generate and create the `.env` file like above.

* `npm install` to install all required dependencies
* `npm start` to start the local server
* `npm run dev-start` to start development local server
* `npm run lint` to check the lint of files