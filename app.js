import dotenv from 'dotenv';
import express from 'express';
import routes from './src/routes';

dotenv.config();
const app = express();

app.use('/', routes);

app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`App listent to port ${port}`));
